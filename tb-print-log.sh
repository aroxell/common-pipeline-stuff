#!/bin/bash

# $1: build.json emitted by tuxbuild's --json-out
buildjson_is_buildset() {
  jq '.[] | .build_key' "$1" > /dev/null 2>&1
}

# $1: build.json emitted by tuxbuild's --json-out
# $2: build_key
# $3: jq filter
# $4: item to obtain
buildjson_get_key() {
  jq -r "${3}select(.build_key == \"${2}\") | .${4}" "${1}"
}

# $1: build.log file
buildjson_print_log() {
  if [ -f "$1" ]; then
    echo "-----v-----v-----$1-----v-----v-----"
    cat "$1"
    echo "-----^-----^-----$1-----^-----^-----"
  fi
}

# $1: build.json emitted by tuxbuild's --json-out
buildjson_print_info() {
  build_json="${1:-build.json}"

  jq_filter=""
  if buildjson_is_buildset "${build_json}"; then
    jq_filter=".[] | "
  fi

  for build_key in $(jq -r "${jq_filter}.build_key" "${build_json}"); do
    for item in \
      build_status \
      download_url \
      errors_count \
      git_sha \
      warnings_count; do
      eval ${item}="$(buildjson_get_key "${build_json}" "${build_key}" "${jq_filter}" "${item}")"
    done

    echo "================================================================================"
    # shellcheck disable=SC2154
    echo -e "Build ID: ${build_key}\\tGit SHA: ${git_sha}"
    # shellcheck disable=SC2154
    echo -e "Errors: ${errors_count}\\tWarnings: ${warnings_count}"

    # shellcheck disable=SC2154
    if [ "${errors_count}" -gt 0 ] || [ "${warnings_count}" -gt 0 ] || [ ! "${build_status}" = "pass" ]; then
      # shellcheck disable=SC2154
      wget -q -O "build-${build_key}.log" "${download_url}build.log" || :
      buildjson_print_log "build-${build_key}.log"
    fi

  done
}

# If directly invoked
if [ "${#BASH_SOURCE[@]}" -eq 1 ]; then
  buildjson_print_info "$*"
fi
