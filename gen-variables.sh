#!/usr/bin/env bash

set -e
#set -x
set -u

# $1 parameter to extract
#    This argument is required
# $2 location of bundle.json
#    This is optional. Defaults to
#    bundle.json in the current
#    directory.
get_from_bundle() {
  bundle_json="bundle.json"
  if [ $# -gt 1 ]; then
    bundle_json="$2"
  fi
  jq -r ".$1" "${bundle_json}"
}

# Generate a file called submit-tests.sh
# to make it easier for other to reuse.
generate_submit_tests() {
  cd "${WORKDIR}/lava-test-plans"
  cat << EOF | tee "${WORKDIR}/submit-tests.sh"
#!/bin/bash -e
[ -d "${WORKDIR}/lava-test-plans" ] && cd "${WORKDIR}/lava-test-plans" || exit 1
python3 submit_for_testing.py \\
  ${DRY_RUN} \\
  --variables "${TEST_PLANS_VAR_FILE}" \\
  --device-type "${DEVICE_TYPE}" \\
  --build-number "${GIT_DESCRIBE}" \\
  --lava-server "${LAVA_SERVER}" \\
  --qa-server "${QA_SERVER}" \\
  --qa-server-team "${QA_TEAM}" \\
  --qa-server-project "${QA_PROJECT}" \\
  ${LAVA_TESTS} \\
  ${testplan_device_path} \\
  ${QA_ENVIRONMENT}
EOF
}

# Given a MACHINE, define DEVICE_TYPE
# $1: MACHINE name. Required
machine_to_devicetype() {
  if [ $# -lt 1 ]; then
    return 1
  fi

  local MACHINE="$1"
  case "${MACHINE}" in
    dragonboard-410c) DEVICE_TYPE="dragonboard-410c" ;;
    hikey)            DEVICE_TYPE="hi6220-hikey" ;;
    intel-core2-32)   DEVICE_TYPE="i386" ;;
    juno)             DEVICE_TYPE="juno-r2" ;;
    ls2088ardb)       DEVICE_TYPE="nxp-ls2088" ;;
    am57xx-evm)       DEVICE_TYPE="x15" ;;
    intel-corei7-64)  DEVICE_TYPE="x86" ;;
    qemu_arm)         DEVICE_TYPE="qemu_arm" ;;
    qemu_arm64)       DEVICE_TYPE="qemu_arm64" ;;
    qemu_i386)        DEVICE_TYPE="qemu_i386" ;;
    qemu_x86_64)      DEVICE_TYPE="qemu_x86_64" ;;
  esac

  export DEVICE_TYPE
}

check_and_write() {
  if [ -v "${1}" ] && [ -n "${1}" ]; then
    echo "${1}=${!1}" >> "${TEST_PLANS_VAR_FILE}"
  fi
}

# Assign a value to a variable if:
# * it doesn't exist, or
# * its value is empty
# $1: Variable name
# $2: Variable value
check_and_set() {
  if [ -v "${1}" ]; then
    if [ -z "${1}" ]; then
      export "${1}"="${2}"
    fi
  else
    export "${1}"="${2}"
  fi
}

# Define variables that are used by submit_for_testing.py
define_sft_vars_for_machine() {
  # DEVICE_TYPE is defined by machine_to_devicetype()
  # QA_PROJECT is defined by create_kernel_vars_for_machine()
  [ ! -v GIT_DESCRIBE ] && [ -v LATEST_SHA ] && GIT_DESCRIBE="${LATEST_SHA:0:12}"
  LAVA_SERVER=https://lkft.validation.linaro.org/RPC2/
  QA_SERVER=https://qa-reports.linaro.org
  TEST_PLANS_VAR_FILE="${WORKDIR}/variables.ini"
  check_and_set QA_TEAM "staging-lkft"

  case "${DEVICE_TYPE}" in
    nxp-ls2088)
      LAVA_SERVER=https://lavalab.nxp.com/RPC2/
      ;;
  esac

  export \
    GIT_DESCRIBE \
    LAVA_SERVER \
    QA_SERVER \
    QA_TEAM \
    TEST_PLANS_VAR_FILE
}

create_rootfs_vars_for_machine() {

  BUNDLED_URL=${1}
  ROOTFS_BUNDLE="${BUNDLED_URL}/bundle.json"

  if [ -n "${bundle_json}" ] && [ -z "${ROOTFS_URL}" ]; then
    case "${ROOTFS_MACHINE}" in
      am57xx-evm)
        case ${MACHINE} in
          am57xx-evm) rootfs_ext="ext4gz" ;;
          juno)       rootfs_ext="tarxz"  ;;
          qemu_arm)   rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      dragonboard-410c)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle rootfs.ext4gz "${bundle_json}")"
        ;;

      hikey)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle rootfs.ext4gz "${bundle_json}")"
        ;;

      intel-core2-32)
        case ${MACHINE} in
          intel-core2-32) rootfs_ext="tarxz" ;;
          qemu_i386)      rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      intel-corei7-64)
        case ${MACHINE} in
          intel-corei7-64) rootfs_ext="tarxz" ;;
          qemu_x86_64)     rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      juno)
        case ${MACHINE} in
          juno)       rootfs_ext="tarxz" ;;
          qemu_arm64) rootfs_ext="ext4gz" ;;
        esac
        # shellcheck disable=SC2034
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      ls2088ardb)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle rootfs.tarxz "${bundle_json}")"
        ;;
    esac
  fi

  check_and_write ROOTFS_URL

}

create_kernel_vars_for_machine() {
  echo
  echo "====================================================="
  echo "Now submitting jobs for ${MACHINE^^}"
  echo "Using ${build_json} for build info"

  unset DTB_FILENAME
  BUNDLED_URL=${1}
  BOOT_BUNDLE="${BUNDLED_URL}/bundle.json"

  if [ -v GITLAB_CI ]; then
    if [ ! -f "${build_json}" ]; then
      echo "Artifact ${build_json} not found. Can't continue."
      exit 1
    fi
    GIT_DESCRIBE="$(jq -r '.[] | .git_describe' "${build_json}")"
    DOWNLOAD_URL="$(jq -r '.[] | .download_url' "${build_json}")"
    # The URL ends with /, so remove the last one
    KERNEL_PUB_DEST="$(echo "${DOWNLOAD_URL}" | cut -d/ -f1-4)"
    BUILD_URL="${CI_PIPELINE_URL}"
    BUILD_NUMBER="${CI_BUILD_ID}"
    build_definition_json="$(download_and_cache "${DOWNLOAD_URL}build_definition.json")"
    MAKE_KERNELVERSION="$(jq -r '.kernel_version' "${build_definition_json}")"
    bmeta_json="$(download_and_cache "${DOWNLOAD_URL}bmeta.json")"
    KERNEL_NAME="$(jq -r '.kernel_image' "${bmeta_json}")"
    MODULES_NAME="$(jq -r '.modules' "${bmeta_json}")"

  else
    KERNEL_NAME=Image
    case "${MACHINE}" in
      am57xx-evm | qemu_arm)
        KERNEL_NAME=zImage
        ;;
      intel-corei7-64 | qemu_x86_64 | intel-core2-32 | qemu_i386)
        KERNEL_NAME=bzImage
        ;;
    esac
  fi

  KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}

  bundle_json="$(download_and_cache "${BOOT_BUNDLE}")"
  if [ -n "${bundle_json}" ] && [ -z "${BOOT_URL}" ]; then
    case "${ROOTFS_MACHINE}" in
      dragonboard-410c)
        BOOT_URL="${BUNDLED_URL}/$(get_from_bundle boot "${bundle_json}")"
        ;;

      hikey)
        BOOT_URL="${BUNDLED_URL}/$(get_from_bundle boot "${bundle_json}")"
        ;;

    esac
  fi
  case "${MACHINE}" in
    dragonboard-410c)
      # Qualcomm's Dragonboard 410c
      DTB_FILENAME=dtbs/qcom/apq8016-sbc.dtb
      ;;
    hikey)
      # HiKey
      DTB_FILENAME=dtbs/hisilicon/hi6220-hikey.dtb
      ;;
    juno | qemu_arm64)
      # Arm's Juno
      DTB_FILENAME=dtbs/arm/juno-r2.dtb
      if [ "${SKIPGEN_KERNEL_VERSION}" = "4.4" ]; then
        DTB_FILENAME=dtbs/arm/juno-r1.dtb
      fi
      if [ "${MACHINE}" = "qemu_arm64" ]; then
        unset DTB_FILENAME
        BOOT_URL=${KERNEL_URL}
      fi
      ;;
    ls2088ardb)
      # NXP's LS2088A RDB
      DTB_FILENAME=dtbs/freescale/fsl-ls2088a-rdb.dtb
      ;;
    am57xx-evm | qemu_arm)
      # am57xx-evm
      DTB_FILENAME=dtbs/am57xx-beagle-x15.dtb
      BOOT_URL=${KERNEL_URL}
      # shellcheck disable=SC2034
      if [ "${MACHINE}" = "qemu_arm" ]; then
        unset DTB_FILENAME
      fi
      ;;
    intel-corei7-64 | qemu_x86_64)
      # intel-corei7-64
      BOOT_URL=${KERNEL_URL}
      ;;
    intel-core2-32 | qemu_i386)
      # intel-core2-32
      # shellcheck disable=SC2034
      BOOT_URL=${KERNEL_URL}
      ;;
  esac

  if [[ $DEVICE_TYPE == *"qemu_"* ]]; then
    # shellcheck disable=SC2034
    OVERLAY_MODULES_URL=${KERNEL_PUB_DEST}/${MODULES_NAME}
  else
    # shellcheck disable=SC2034
    MODULES_URL=${KERNEL_PUB_DEST}/${MODULES_NAME}
  fi

  cat << EOF >> "${TEST_PLANS_VAR_FILE}"
DEVICE_TYPE=${DEVICE_TYPE}
KERNEL_PUB_DEST=${KERNEL_PUB_DEST}
BUILD_NUMBER=${BUILD_NUMBER}
BUILD_URL=${BUILD_URL}
KERNEL_URL=${KERNEL_URL}
KERNEL_CONFIG_URL=${KERNEL_PUB_DEST}/kernel.config
#
KERNEL_DESCRIBE=${GIT_DESCRIBE}
KERNEL_COMMIT=${LATEST_SHA}
MAKE_KERNELVERSION=${MAKE_KERNELVERSION}
SKIPGEN_KERNEL_VERSION=${SKIPGEN_KERNEL_VERSION}
KERNEL_BRANCH=${KERNEL_BRANCH}
KERNEL_REPO=${KERNEL_REPO}
EOF

  if [ -v DTB_FILENAME ] && [ -n "${DTB_FILENAME}" ]; then
    echo "DTB_URL=${KERNEL_PUB_DEST}/${DTB_FILENAME}" >> "${TEST_PLANS_VAR_FILE}"
  fi

  for var in \
    BOOT_URL \
    DTB_FILENAME \
    MODULES_URL \
    OVERLAY_MODULES_URL \
    LAVA_TEST_NAME_SUFFIX; do
    check_and_write ${var}
  done

}

project_specific_variables() {
  cat << EOF >> "${TEST_PLANS_VAR_FILE}"
# hikey:
# common, need adjustment per project:
PROJECT_NAME=${QA_PROJECT}
KSELFTEST_PATH=/opt/kselftests/mainline/
LAVA_JOB_PRIORITY=${LAVA_JOB_PRIORITY}
TDEFINITIONS_REVISION=master
PROJECT=projects/lkft/
TAGS=${LAVA_TAGS}
KVM_UNIT_TESTS_REVISION=f2606a873e805f9aff4c4879ec75e65d7e30af73
EOF
}

# $1: Branch, in the format "linux-4.14.y", "mainline", "next". Required.
# $2: DEVICE_TYPE. Optional.
# $3: QA_PROJECT. Optional.
# From the environment:
#   LAVA_JOB_PRIORITY: If defined, it will return this value.
get_branch_priority() {
  local branch=""
  local device_type=""
  local qa_project=""

  # Can be overridden via environment
  if [ -v LAVA_JOB_PRIORITY ]; then
    echo "${LAVA_JOB_PRIORITY}"
    return
  fi

  if [ $# -lt 1 ]; then
    return 1
  fi

  branch=$1

  if [ $# -eq 3 ]; then
    device_type="$2"
    qa_project="$3"
  fi

  prio=25

  case ${branch} in
    # linux and linux-next uses master
    master)          prio=35 ;;
    linux-4.4.y)     prio=71 ;;
    linux-4.9.y)     prio=72 ;;
    linux-4.14.y)    prio=73 ;;
    linux-4.19.y)    prio=74 ;;
    linux-5.4.y)     prio=75 ;;
    linux-5.5.y)     prio=77 ;;
    linux-5.6.y)     prio=78 ;;
  esac

  echo ${prio}
}

# Get the hash of any string
hash_string() {
  echo -n "$1" | sha1sum | cut -c1-40
}

# $1: URL to download and cache
# From the environment:
#   WORKSPACE
#
# Returns the absolute path and file name of the downloaded resource.
download_and_cache() {
  hashed_url="$(hash_string "${1}")"
  output_file="${WORKDIR}/.downloads/${hashed_url}"

  mkdir -p "${WORKDIR}/.downloads/"

  if [ ! -s "${output_file}" ]; then
    wget -O "${output_file}" "${1}"
  fi
  if [ -s "${output_file}" ]; then
    readlink -e "${output_file}"
  fi
}

# START HERE

if [[ -v CI && ! ${CI_PIPELINE_SOURCE} == "merge_request_event" ]]; then
  env | sort
  echo
fi

LAVA_TESTS=${LAVA_TESTS:-"--test-plan lkft-sanity"}
testplan_device_path="--testplan-device-path projects/lkft/devices"
LAVA_TEST_PLANS_URL=${LAVA_TEST_PLANS_URL:-"https://github.com/linaro/lava-test-plans"}
LAVA_TEST_PLANS_ORIGIN=$(echo "${LAVA_TEST_PLANS_URL}"| awk -F'/' '{print $(NF-1)}')
[[ "${LAVA_TEST_PLANS_ORIGIN}" == "linaro" ]] && LAVA_TEST_PLANS_ORIGIN="origin"
LAVA_TEST_PLANS_BRANCH=${LAVA_TEST_PLANS_BRANCH:-"master"}

if [ -v DRY_RUN ]; then
  DRY_RUN="--dry-run"
else
  DRY_RUN=""
fi
export DRY_RUN

if [ -v CI ]; then
  WORKDIR="${CI_PROJECT_DIR}"
else
  WORKDIR="$(dirname "$(readlink -e "$0")")/workspace"
fi
mkdir -p "${WORKDIR}"

if [ ! -d "${WORKDIR}/lava-test-plans" ]; then
  git clone -o origin https://github.com/linaro/lava-test-plans "${WORKDIR}/lava-test-plans"
else
  if [ -v GIT_SYNC ] || [ -v CI ]; then
    cd "${WORKDIR}/lava-test-plans"
    git fetch ${LAVA_TEST_PLANS_ORIGIN}
    git reset --hard ${LAVA_TEST_PLANS_ORIGIN}/${LAVA_TEST_PLANS_BRANCH}
  fi
fi

echo "lava-test-plans revision $(git -C "${WORKDIR}/lava-test-plans/" rev-parse HEAD)"

if [ -v CI ]; then
  pip install -r "${WORKDIR}/lava-test-plans/requirements.txt"
fi

if [ -v MACHINE ]; then
  # Define MACHINE and DEVICE_TYPE
  machine_to_devicetype "${MACHINE}"

  case "${MACHINE}" in
    qemu_arm)    ROOTFS_MACHINE="am57xx-evm" ;;
    qemu_arm64)  ROOTFS_MACHINE="juno" ;;
    qemu_i386)   ROOTFS_MACHINE="intel-core2-32" ;;
    qemu_x86_64) ROOTFS_MACHINE="intel-corei7-64" ;;
  esac

  # Variables that can be overrided...
  # =======================================================================
  check_and_set ROOTFS_MACHINE "${MACHINE}"
  check_and_set BOOT_URL ""
  check_and_set ROOTFS_URL ""
  check_and_set LAVA_TAGS "production"
  if [ ! -v QA_ENVIRONMENT ]; then
    QA_ENVIRONMENT=""
  else
    QA_ENVIRONMENT="--environment ${QA_ENVIRONMENT}"
  fi
  # =======================================================================

  build_json="build.json"
  if [ $# -ge 1 ]; then
    build_json=$1
  fi

  # Define variables for submit_for_testing
  define_sft_vars_for_machine
  rm -f "${TEST_PLANS_VAR_FILE}"

  BUNDLED_URL="https://storage.lkft.org/rootfs/oe-sumo/20200521/${ROOTFS_MACHINE}"
  # Write kernel related variables
  create_kernel_vars_for_machine ${BUNDLED_URL}

  # Define LAVA job priority
  lava_job_priority="$(get_branch_priority "${KERNEL_BRANCH}" "${DEVICE_TYPE}" "${QA_PROJECT}")"
  export LAVA_JOB_PRIORITY=${lava_job_priority}

  # Write rootfs related variables
  create_rootfs_vars_for_machine ${BUNDLED_URL}

  # Write project specific variables
  project_specific_variables

  echo
  echo "---vvv------${TEST_PLANS_VAR_FILE}------vvv---"
  cat "${TEST_PLANS_VAR_FILE}"
  echo "---^^^------${TEST_PLANS_VAR_FILE}------^^^---"

  generate_submit_tests
fi
